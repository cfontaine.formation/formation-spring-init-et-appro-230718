package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "auteurs")
public class Auteur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 50, nullable = false)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;

    @Column(nullable = false)
    private LocalDate naissance;

    private LocalDate deces;

    @ManyToOne
    private Nation nationalite;

    @ManyToMany(mappedBy = "auteurs")
    private List<Livre> livres = new ArrayList<>();

    public Auteur() {
    }

    public Auteur(String prenom, String nom, LocalDate naissance, LocalDate deces) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.deces = deces;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getNaissance() {
        return naissance;
    }

    public void setNaissance(LocalDate naissance) {
        this.naissance = naissance;
    }

    public LocalDate getDeces() {
        return deces;
    }

    public void setDeces(LocalDate deces) {
        this.deces = deces;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Nation getNationalite() {
        return nationalite;
    }

    public void setNationalite(Nation nationalite) {
        this.nationalite = nationalite;
    }

    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Auteur [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", naissance=" + naissance + ", deces="
                + deces + "]";
    }

}
