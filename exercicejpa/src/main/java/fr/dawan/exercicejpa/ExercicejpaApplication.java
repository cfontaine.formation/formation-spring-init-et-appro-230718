package fr.dawan.exercicejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicejpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicejpaApplication.class, args);
	}

}
