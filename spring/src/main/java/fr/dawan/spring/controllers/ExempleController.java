package fr.dawan.spring.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.spring.entities.Personne;
import fr.dawan.spring.service.EmailService;
import jakarta.inject.Provider;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("/exemple")
@SessionAttributes("user2")
//-> @RequestMapping au niveau du controller: l'url préfixera toutes url des méthodes
public class ExempleController {

    @Autowired
    private EmailService service;

    private static int cpt;
    
    @Autowired
    private Provider<Personne> provider;

    @GetMapping
    public String exemple() {
        return "exemple";
    }

    // @RequestMapping => utilisée pour mapper les requêtes HTTP aux méthodes du
    // contrôleur
    // Model=> utilisé pour transférer des données entre la vue et le contrôleur
    @RequestMapping(value = { "/testmodel", "/model" }, method = RequestMethod.GET)
    public String testModel(Model model) {
        String message = "Test Model";
        model.addAttribute("msg", message);
        return "exemple";
    }

    // ModelAndView => permet de transmettre le nom de la vue et les attributs avec
    // un seul return
    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        // On peut passer le nom de la vue au constructeur
        ModelAndView mdv = new ModelAndView(/* "exemple" */);
        mdv.addObject("msg", "Test Model And View");
        mdv.setViewName("exemple"); // ou utiliser la méthode setViewNam
        return mdv;
    }

    // params -> la méthode sera éxécutée
    // - si la requête a pour url /exemple/testparams
    // - et si la requéte a un paramètre id
    // (http://localhost:8080/exemple/testparams?id=4)
    @GetMapping(value = "/testparams", params = "id")
    public String testParams(Model model) {
        model.addAttribute("msg", "La requête contient le paramètre id");
        return "exemple";
    }

    // (http://localhost:8080/exemple/testparams?size=4&page=5)
    @GetMapping(value = "/testparams", params = { "size", "page" })
    public String testParams2(Model model) {
        model.addAttribute("msg", "La requête contient les paramètres size et page");
        return "exemple";
    }

    // params -> la méthode sera éxécutée :
    // - si la requête a pour url /exemple/testparams
    // - et si la requéte a un paramètre id et qu'il a pour valeur 42
    // (http://localhost:8080/exemple/testparams?id=42)
    @GetMapping(value = "/testparamsval", params = "id=42")
    public String testParamsVal(Model model) {
        model.addAttribute("msg", "La requête contient le paramètre id = 42");
        return "exemple";
    }

    @GetMapping(value = "/testheaders", headers = "User-Agent")
    public String testHeaders(Model model) {
        model.addAttribute("msg", "La requête contient en en-tête le paramètre user-agent");
        return "exemple";
    }

    // Pathvariable
    // (http://localhost:8080/exemple/testpath/10)
    @GetMapping("/testpath/{id}")
    public String testPath(@PathVariable("id") String idVal, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id= " + idVal);
        return "exemple";
    }

    // (http://localhost:8080/exemple/testpathimpl/10)
    @GetMapping("/testpathimpl/{id}")
    public String testPathImpl(@PathVariable String id, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id= " + id);
        return "exemple";
    }

    // (http://localhost:8080/exemple/testpathmulti/42/action/ajout)
    @GetMapping("/testpathmulti/{id}/action/{action}")
    public String testPathMulti(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id= " + id + "et action=" + action);
        return "exemple";
    }

    // (http://localhost:8080/exemple/testpathmap/42/action/ajout)
    @GetMapping("/testpathmap/{id}/action/{action}")
    public String testPathMap(@PathVariable Map<String, String> m, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id= " + m.get("id") + "et action=" + m.get("action"));
        return "exemple";
    }

    // @PathVariable => on peut lever les ambiguités en utilisant des expressions
    // régulières
    @GetMapping("/testpathamb/{id:[0-9]+}") // id-> uniquement un nombre entier positif
    public String testPathAmb(@PathVariable String id, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre id= " + id);
        return "exemple";
    }

    @GetMapping("/testpathamb/{nom:[A-Za-z]+}") // name -> uniquement des lettres minuscules et majuscule
    public String testPathAmb2(@PathVariable String nom, Model model) {
        model.addAttribute("msg", "l'url contient un paramètre nom= " + nom);
        return "exemple";
    }

    // required -> pour rendre @PathVariable optionnel
    @GetMapping({ "/testpathoption/{id}", "/testpathoption" })
    public String testPathOptionnel(@PathVariable(required = false) String id, Model model) {
        if (id == null) {
            model.addAttribute("msg", "l'url ne contient pas de paramètre");
        } else {
            model.addAttribute("msg", "l'url contient un paramètre id= " + id);
        }
        return "exemple";
    }

    // @RequestParam
    @GetMapping("/testparam")
    public String testParam(@RequestParam("id") String idVal, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + idVal);
        return "exemple";
    }

    @GetMapping("/testparamimpl")
    public String testParamImpl(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + id);
        return "exemple";
    }

    @GetMapping("/testparammulti")
    public String testParamImpl(@RequestParam String id, @RequestParam String nom, Model model) {
        model.addAttribute("msg", "paramètres de requete id= " + id + " nom= " + nom);
        return "exemple";
    }

    // @RequestParam => on peut lever les ambiguités en utilisant l'attribut params
    @GetMapping(value = "/testparamamb", params = "id") // on peut entrer dans la méthode uniquement, si la requête
                                                        // contient un paramètre id
    public String testParamAmb1(@RequestParam String id, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + id);
        return "exemple";
    }

    @GetMapping(value = "/testparamamb", params = "nom") // on peut entrer dans la méthode uniquement, si la requête
                                                         // contient un paramètre nom
    public String testParamAmb2(@RequestParam String nom, Model model) {
        model.addAttribute("msg", "paramètre de requete nom= " + nom);
        return "exemple";
    }

    // @RequestParam => l'attribut defaultValue permet de définir une valeur par
    // défaut pour le paramétre
    @GetMapping("/testparamdefault")
    public String testParamDefault(@RequestParam(defaultValue = "42") String id, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + id);
        return "exemple";
    }

    // @RequestParam => l'attribut required permet de rendre le paramètre optionnel
    @GetMapping("/testparamoption")
    public String testPAramOption(@RequestParam(required = false) String id, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + id);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testParamConv(@RequestParam int id, Model model) {
        model.addAttribute("msg", "paramètre de requete id= " + id);
        return "exemple";
    }

    @GetMapping("/testparamconvd")
    public String testParamConvDate(@DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam LocalDate date, Model model) {
        model.addAttribute("msg", "paramètre de requete date= " + date.toString());
        return "exemple";
    }

    @PostMapping("/testformulaire")
    public String testParamFormumaire(@RequestParam String nom, Model model) {
        model.addAttribute("msg", nom);
        return "exemple";
    }

    // Lier des beans

    // (http://localhost:8080/exemple/testbindparam?id=1&prenom=alan&nom=smithee)
    @GetMapping("/testbindparam")
    public String testBindParam(Personne per, Model model) {
        model.addAttribute("msg", per.toString());
        return "exemple";
    }

    // (http://localhost:8080/exemple/testbindpath/2/jane/doe)
    @GetMapping("/testbindpath/{id}/{prenom}/{nom}")
    public String testBindPath(Personne per, Model model) {
        model.addAttribute("msg", per.toString());
        return "exemple";
    }

    @GetMapping({ "/exemplethymeleaf/{val}", "/exemplethymeleaf" })
    public String testThymeleaf(@PathVariable(required = false) Integer val, Model model) {
        int i = 42;
        model.addAttribute("i", i);

        Personne p = new Personne(1, "John", "Doe");
        model.addAttribute("per1", p);

        double tab[] = { 1.23, 4.5, 6.7, 8.9 };
        model.addAttribute("tab", tab);

        Map<String, Integer> m = new HashMap<>();
        m.put("john", 42);
        m.put("marcel", 23);
        model.addAttribute("m", m);

        model.addAttribute("val", val);

        model.addAttribute("html", "<b>un texte</b> ");

        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne(1, "John", "Doe"));
        personnes.add(new Personne(2, "Jane", "Doe"));
        personnes.add(new Personne(3, "Alan", "Smithee"));
        personnes.add(new Personne(4, "Yves", "Rouleau"));
        model.addAttribute("personnes", personnes);

        model.addAttribute("str", "hello world");
        model.addAttribute("today", new Date());

        return "exemplethymeleaf";
    }

    // @RequestHeader => permet de récupérer un paramètre dans l'en-tête de la
    // requête
    @GetMapping("/testheader")
    public String testHeader(@RequestHeader("user-agent") String usergAgent, Model model) {
        model.addAttribute("msg", usergAgent);
        return "exemple";
    }

    // HttpHeaders => pour récupérer tous les paramètres de la requête
    @GetMapping("/testallheader")
    public String testAllHeader(@RequestHeader HttpHeaders headers, Model model) {
        List<String> lstHeader = new ArrayList<>();
        Set<Entry<String, List<String>>> entry = headers.entrySet();
        for (Entry<String, List<String>> e : entry) {
            String tmp = e.getKey() + " :";
            for (String v : e.getValue()) {
                tmp += v + " ";
            }
            lstHeader.add(tmp);
        }
        model.addAttribute("lstHeader", lstHeader);
        return "exemple";
    }

    // Redirection
    @GetMapping("/testredirect")
    public String testRedirection() {
        return "redirect:/hello"; // 302
    }

    @GetMapping("/testforward")
    public String testForward() {
        return "forward:/hello";
    }

    // @ModelAttribute sur une méthode
    @ModelAttribute("cpt")
    public int testModelAttribut() {
        return cpt++;
    }
//   ou
//
//    @ModelAttribute
//    public void testModelAttribut2(Model model) {
//        model.addAttribute("cpt", cpt++);
//    }

    // @ModelAttribute sur un paramètre de méthode
    @GetMapping("/testmodelattrparam")
    public String testModelAttributeParam(@ModelAttribute("per1") Personne p1, Model model) {
        model.addAttribute("msg", p1.toString());
        return "exemple";
    }

    @ModelAttribute("per1")
    public Personne initPer1() {
        return new Personne(4, "John", "Doe");
    }

    // FlashAttribute
    @GetMapping("/testflash")
    public String testFlashAttribue(RedirectAttributes rAtt) {
        rAtt.addFlashAttribute("msgFlash", "Vous venez d'être redirigé");
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/cibleflash")
    public String cibleFlash(@ModelAttribute("msgFlash") String msgFlash, Model model) {
        model.addAttribute("msg", msgFlash);
        return "exemple";
    }

    // Gestion des exceptions
    @GetMapping("/ioexception")
    public void genIOException() throws IOException {
        throw new IOException("IO Exception");
    }

    @GetMapping("/sqlexception")
    public void genSQLException() throws SQLException {
        throw new SQLException("SQL Exception");
    }

    @ExceptionHandler(IOException.class)
    public String handlerIoException(Exception e, Model model) {
        model.addAttribute("msgEx", e.getMessage());
        model.addAttribute("traceEx", e.getStackTrace());
        return "exception";
    }

    @GetMapping("/simplemail")
    public String sendSimpleMail() {
        service.sendSimpleMail("Le text du mail", "un mail de test", "jd@dawan.com", "no-reply@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/htmlmail")
    public String sendHtmlMail() {
        String mail = "<html><body><h1>Un mail en HTML</h1></body></html>";
        service.sendHtmlMail(mail, "un mail de test", "jd@dawan.com", "no-reply@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/templatemail")
    public String sendTemplateMail() {
        Map<String, Object> model = new HashMap<>();
        model.put("prenom", "John");
        model.put("email", "jdoe@dawan.com");
        service.sendTemplateMail("mailTemplate.html", model, "un mail de test", "jd@dawan.com", "no-reply@dawan.com");
        return "redirect:/exemple";
    }

    @GetMapping("/attachementmail")
    public String sendAttachementMail() {
        service.sendMailAttachement("mail de test", "un mail de test", "jd@dawan.com", "no-reply@dawan.com",
                new File("logo.jpg"));
        return "redirect:/exemple";
    }

    // Upload
    @PostMapping("/upload")
    String testUpload(@RequestParam("imgfile") MultipartFile mpf) {
        System.out.println(mpf.getOriginalFilename() + " " + mpf.getSize());
        File rep = new File("C:/Dawan/upload");
        if (!rep.exists()) {
            rep.mkdir();
        }
        try (BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream("C:/Dawan/upload/" + mpf.getOriginalFilename()))) {
            bos.write(mpf.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/exemple";
    }
    
    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse response) {
        Cookie c=new Cookie("testcookie","valeur_de_test");
        c.setMaxAge(30);
        response.addCookie(c);
        return "exemple";
    }
    
    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(value="testcookie",defaultValue="valeur_par_defaut") String value,Model model ) {
        model.addAttribute("msg", "Cookie = "+value);
        return "exemple";
    }
    
    @GetMapping("/readallcookie")
    public String readCookieAll(HttpServletRequest request,Model model) {
        Cookie [] cookies=request.getCookies();
        List<String> lstCookies=new ArrayList<>();
        for(Cookie c : cookies) {
            lstCookies.add(c.getName() + " : "+c.getValue());
        }
        model.addAttribute("cookies", lstCookies);
        return "exemple";
        
    }
    
    // Session
    // Méthode 1 : Session Java EE
    @GetMapping("/writesession1")
    public String writeSession1(HttpServletRequest request,Model model ) {
        HttpSession session=request.getSession();
        session.setAttribute("user1", new Personne(1,"John","Doe"));
        model.addAttribute("msg", "Ajout Personne Session Méthode1");
        return "exemple";
    }
    
    @GetMapping("/readsession1")
    public String readSession1(HttpServletRequest request,Model model ) {
        HttpSession session=request.getSession();
        Personne per1=(Personne)session.getAttribute("user1");
        if(per1!=null) {
            model.addAttribute("msg", "Sesion1 "+  per1.getPrenom() + " "+ per1.getNom());
        }
        return "exemple";
    }
    
    // Methode 2 @SessionAttributes
    @GetMapping("/writesession2")
    public String writeSession2(Model model ) {
        model.addAttribute("user2", new Personne(1,"Jane","Doe"));
        model.addAttribute("msg", "Ajout Personne Session Méthode2");
        return "exemple";
    }
    
    @ModelAttribute("user2")
    public Personne initSession2() {
        return new Personne();
    }
    
    @GetMapping("/readsession2")
    public String readSession2(@ModelAttribute("user2") Personne per2,Model model ) {
        model.addAttribute("msg", "Sesion2 "+  per2.getPrenom() + " "+ per2.getNom());
         return "exemple";
    }
    
    // Méthode 3 spring bean scope session
    
    @GetMapping("/writesession3")
    public String writeSession3(Model model ) {
        Personne user3=provider.get();
        user3.setPrenom("Alan");
        user3.setNom("Smithee");
        model.addAttribute("msg", "Ajout Personne Session Méthode3");
        return "exemple";
    }
    
    @GetMapping("/readsession3")
    public String readSession3(Model model ) {
        Personne user3=provider.get();;
        model.addAttribute("msg", "Sesion3"+  user3.getPrenom() + " "+ user3.getNom());

        return "exemple";
    }
    
}
