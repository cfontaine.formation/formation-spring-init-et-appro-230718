package fr.dawan.spring.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
// Controlleur pour traiter les exceptions au niveau de l'application
@ControllerAdvice
public class ExceptionController {
    
    
    @ExceptionHandler
    public String handlerException(Exception e,Model model) {
        model.addAttribute("msgEx", e.getMessage() + " (Application)");
        model.addAttribute("traceEx", e.getStackTrace());
        return "exception";
    }

}
