package fr.dawan.spring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.spring.entities.Article;
import fr.dawan.spring.repositories.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleControllerWS {

   @Autowired
  // private  FakeArticleRepository repository;
   private  ArticleRepository repository;
   
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getAllArticles(){
       return repository.findAll();
    }
    
    
    @GetMapping(params= {"size","page"},produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getAllArticles(Pageable page){
       return repository.findAll(page).getContent();
    }
    
    
    @GetMapping(value= "/{id}", produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Article> getArticleById(@PathVariable long id) {
        try {
            return ResponseEntity.ok(repository.findById(id).get());
        } catch (Exception e) {
          return ResponseEntity.notFound().build();
        }
    }
    
    
    
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public Article addArticle(@RequestBody Article a) {
        return repository.save(a);
    }
    
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteArticle(@PathVariable long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            return new ResponseEntity<>("L'article id=" +id+ " n'existe pas",HttpStatus.NOT_FOUND) ;
        }
        return new ResponseEntity<>("L'article id=" +id+ " est supprimée",HttpStatus.OK) ;
    }
    
    @PutMapping(value="/{id}", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public Article updateArticle(@PathVariable long id, @RequestBody Article a) {
           Article a1=repository.findById(id).get();
           a1.setPrix(a.getPrix());
           a1.setDescription(a.getDescription());
           a1.setDateProduction(a.getDateProduction());
           repository.save(a1);
           return a1;
           
    }
}
