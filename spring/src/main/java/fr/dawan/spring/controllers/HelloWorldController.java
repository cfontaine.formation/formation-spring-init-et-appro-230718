package fr.dawan.spring.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
    
    // @RequestMapping => utilisée pour mapper les requêtes HTTP aux méthodes du contrôleur
    @RequestMapping("/hello")
    public String helloWorld() {
        return "helloworld"; // on retourne le nom de la vue
    }

}
