package fr.dawan.spring.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.spring.entities.Article;
import fr.dawan.spring.entities.User;
import fr.dawan.spring.forms.FormArticle;
import fr.dawan.spring.forms.FormUser;
import fr.dawan.spring.forms.UserValidator;
import fr.dawan.spring.repositories.ArticleRepository;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
   private ArticleRepository repository;
  //  private FakeArticleRepository repository;
    
    @GetMapping("/articles")
    public String displayArticle(Model model) {
        List<Article> articles=repository.findAll();
        model.addAttribute("articles", articles);
        return "articles";
    }
    
    @GetMapping("/articles/delete/{id}")
    public String deleteArticle(@PathVariable long id) throws Exception {
      //  repository.remove(id);
        repository.deleteById(id);
        return "redirect:/admin/articles";
    }
    
    @GetMapping("/articles/add")
    public String addArticle(@ModelAttribute("formarticle") FormArticle formarticle ) {
        return "addarticle";
    }
    
    
    @PostMapping("/articles/add")
    public String addArticlePost(@Valid @ModelAttribute("formarticle")FormArticle formarticle,BindingResult results,Model model) {
        if(results.hasErrors()) {
            model.addAttribute("formarticle", formarticle);
            model.addAttribute("errors", results);
            return "addarticle";
        }
        else {
            Article article=new Article(formarticle.getDescription(),formarticle.getPrix(),formarticle.getDateProduction());
          //  repository.saveOrUpdate(article);
            repository.saveAndFlush(article);
            return "redirect:/admin/articles";
        }
    }
    
    @GetMapping("/articles/exportcsv")
    public String exportCsv(HttpServletResponse response) {
        response.setContentType("text/csv");
        response.setHeader("ContentDiposition","attachement;filname=articles.csv");
        List<Article> lst=repository.findAll();
        try {
            ServletOutputStream sos=response.getOutputStream();
            sos.write(("id;description;prix;dateproduction\n").getBytes());
            for(Article a: lst) {
                sos.write((a.getId() + ";" +a.getDescription() +";" + a.getPrix()+";"+a.getDateProduction().toString()+"\n").getBytes());
            }
            sos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return "redirect:/admin/articles";
    }
    
    
    @GetMapping("/users/add")
    public String addUser(@ModelAttribute("formuser") FormUser formuser ) {
        return "adduser";
    }
    

    @PostMapping("/users/add")
    public String addUserPost(@Valid @ModelAttribute("formuser") FormUser formuser,BindingResult results,Model model) {
       new UserValidator().validate(formuser, results);
        if(results.hasErrors()) {
            model.addAttribute("formuser", formuser);
            model.addAttribute("errors", results);
            return "adduser";
        }
        else {
            User u=new User(formuser.getPrenom(),formuser.getNom(),formuser.getDateNaissance(),formuser.getEmail());
            u.setPassword(formuser.getPassword());
            System.out.println("Ajouter dans la bdd: "+u);
            return "redirect:/exemple";
        }
    }
}
