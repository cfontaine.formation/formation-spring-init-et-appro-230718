package fr.dawan.spring.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;

// Controleur pour gérer la page d'erreur (4xx et 5xx)
@Controller
public class MyErrorController implements ErrorController {

    // L'url qui sera contacté en cas d'erreur est définie dans
    // application.properties avec la propriété: server.error.path
    @GetMapping("/erreur")
    public String handlerError(HttpServletRequest request, Model model) {
        // Récupération du code status de l'erreur
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            int codeStatus = Integer.parseInt(status.toString());
            switch (codeStatus) {
            case 404:
                model.addAttribute("msgErr", "La page est introuvable");
                break;
            case 401:
                model.addAttribute("msgErr", "Vous n'étes pas authentifié");
                break;
            case 403:
                model.addAttribute("msgErr", "Vous n'avez pas l'autorisation");
                break;
            case 500:
                model.addAttribute("msgErr", "Erreur interne");
                break;
            default:
                model.addAttribute("msgErr", "Une erreur c'est produite");
            }
            model.addAttribute("codeStatus", codeStatus);
        }
        return "error";
    }
}
