package fr.dawan.spring.forms;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;

public class FormUser {

    @NotEmpty(message= "ne doit pas être vide !!!!!!")
    @Size(max=60)
    private String prenom;
    
    @NotEmpty
    @Size(min=2,max=60)
    private String nom;
    
    @Past
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NotNull
    private LocalDate dateNaissance;
    
    @NotEmpty
    @Email
    private String email;
    
    @NotEmpty
    @Size(min=6,max=50)
    private String password;
    
    @NotEmpty
    @Size(min=6,max=50)
    private String passwordConf;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConf() {
        return passwordConf;
    }

    public void setPasswordConf(String passwordConf) {
        this.passwordConf = passwordConf;
    }
    
    
}
