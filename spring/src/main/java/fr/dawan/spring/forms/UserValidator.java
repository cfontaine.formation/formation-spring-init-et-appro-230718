package fr.dawan.spring.forms;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return  clazz==FormUser.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        FormUser formUser=(FormUser)target;
        if(formUser.getPassword()==null || formUser.getPasswordConf()==null ||
                !formUser.getPassword().equals(formUser.getPasswordConf())) {
            formUser.setPassword("");
            formUser.setPassword("");
            errors.rejectValue("passwordConf", "user.passwordConf.NotEquals","password and comfirm password are not equals");
        }
    }

}
