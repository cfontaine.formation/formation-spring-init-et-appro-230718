package fr.dawan.spring.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
@Entity
@Table(name="fournisseurs")
public class Fournisseur implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false,length = 60)
    private String nom;
    
    @ManyToMany(mappedBy="fournisseurs")
    private List<Article> articles=new ArrayList<>();

    public Fournisseur() {
        super();
    }

    public Fournisseur(String nom) {
        super();
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Fournisseur [id=" + id + ", version=" + version + ", nom=" + nom + "]";
    }
    
}
