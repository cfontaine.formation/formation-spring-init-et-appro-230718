package fr.dawan.spring.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name="articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;
    
    @Column(length = 150,nullable=false)
    private String description;

    @Column(nullable=false)
    private double prix;
    
    @Column(name="date_production",nullable=false)
    private LocalDate dateProduction;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false,length = 10)
    private Conditionnement emballage;
    
    @Lob
    @Column(length = 66000)
    private byte[] photo;
    
    @ManyToOne
    private Marque marque;
    
    @ManyToMany
    private List<Fournisseur> fournisseurs=new ArrayList<>();
    
    public Article() {

    }

    public Article(String description, double prix, LocalDate dateProduction) {
        this.description = description;
        this.prix = prix;
        this.dateProduction = dateProduction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Conditionnement getEmballage() {
        return emballage;
    }

    public void setEmballage(Conditionnement emballage) {
        this.emballage = emballage;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", version=" + version + ", description=" + description + ", prix=" + prix
                + ", dateProduction=" + dateProduction + ", emballage=" + emballage + "]";
    }

}
