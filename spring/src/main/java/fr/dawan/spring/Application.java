package fr.dawan.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import fr.dawan.spring.entities.Personne;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);  
	}
	
	@Bean
	@Scope("session")
	Personne user3() {
	    return new Personne();
	}

}
