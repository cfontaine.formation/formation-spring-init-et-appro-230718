package fr.dawan.spring.service;

import java.io.File;
import java.util.Map;

public interface EmailService {

    void sendSimpleMail(String content, String title, String to, String from);

    void sendHtmlMail(String content, String title, String to, String from);

    void sendTemplateMail(String template, Map<String, Object> modelMap, String title, String to, String from);

    void sendMailAttachement(String content, String title, String to, String from, File file);
}
