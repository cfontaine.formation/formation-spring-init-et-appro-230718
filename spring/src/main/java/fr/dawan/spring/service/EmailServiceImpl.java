package fr.dawan.spring.service;

import java.io.File;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMessage.RecipientType;

@Service
public class EmailServiceImpl implements EmailService {
    
    @Autowired
    JavaMailSender sender;
    
    @Autowired
    TemplateEngine templateEngine;

    @Override
    public void sendSimpleMail(String content, String title, String to, String from) {
        SimpleMailMessage message=new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(title);
        message.setText(content);
        sender.send(message);
    }

    @Override
    public void sendHtmlMail(String content, String title, String to, String from) {
        sender.send(new MimeMessagePreparator() {
            
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(from);
                mimeMessage.setSubject(title, "utf-8");
                mimeMessage.setText(content,"utf-8","html");
            }
        });

    }

    @Override
    public void sendTemplateMail(String template, Map<String, Object> modelMap, String title, String to, String from) {
        Context context=new Context();
        context.setVariables(modelMap);
        String htmlContent=templateEngine.process(template, context);
        sendHtmlMail(htmlContent, title, to, from);
    }

    @Override
    public void sendMailAttachement(String content, String title, String to, String from, File file) {
        sender.send(new MimeMessagePreparator() {
            
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message= new MimeMessageHelper(mimeMessage,true,"utf-8");
                message.setTo(to);
                message.setFrom(from);
                message.setSubject(title);
                message.setText(content);
                message.addAttachment(file.getName(),file);
            }
        });

    }

}
