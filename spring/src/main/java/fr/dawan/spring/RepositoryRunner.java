package fr.dawan.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.spring.entities.Article;
import fr.dawan.spring.repositories.ArticleRepository;

@Component
public class RepositoryRunner implements ApplicationRunner {

    @Autowired
    ArticleRepository repo;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Article> lst=repo.findAll();
        for(Article a: lst) {
            System.out.println(a);
        }
        System.out.println("-------------------");
        lst=repo.findByPrixLessThan(60.0);
        for(Article a: lst) {
            System.out.println(a);
        }
        System.out.println("-------------------");
        lst=repo.findByDescriptionLike("T__%");
        for(Article a: lst) {
            System.out.println(a);
        }
        System.out.println("-------------------");
        lst=repo.findByPrixLessThanOrderByPrixAscDateProduction(1000.0);
        for(Article a: lst) {
            System.out.println(a);
        }
        System.out.println("-------------------");
        lst=repo.findByMarqueNomIgnoreCase("marque a");
        for(Article a: lst) {
            System.out.println(a);
        }
        
        System.out.println("-------------------");
        System.out.println(repo.findTopByOrderByPrixDesc());

        
        System.out.println("-------------------");
        lst=repo.findByPrixLessThan(1000.0, PageRequest.of(1, 2));
        for(Article a: lst) {
            System.out.println(a);
        }
        
        System.out.println("-------------------");
        Page<Article>p=repo.findByPrixGreaterThan(1.0, PageRequest.of(1, 2));
        System.out.println(p.getTotalElements());
        System.out.println(p.getTotalPages());
        lst=p.getContent();
        for(Article a: lst) {
            System.out.println(a);
        }
        
    }

}
