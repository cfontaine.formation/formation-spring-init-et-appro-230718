package fr.dawan.spring.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.spring.entities.Article;


public interface ArticleRepository extends JpaRepository<Article,Long> {

    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixBetween(double prixMin,double prixMax);
    
    List<Article> findByPrixGreaterThanAndDateProductionAfter(double prixMin,LocalDate dateProd);
    
    List<Article> findByDescriptionLike(String pattern);
    
    List<Article> findByPrixLessThanOrderByPrixAscDateProduction(double prix);
    
    // Expression de chemin uniquement avec @ManyToOne @OneToOne
    List<Article> findByMarqueNomIgnoreCase(String nomMarque);
    
    // Ne fonctionne pas avec @OneToMany @ManyToMany
    // List<Article> findByFournisseurNom(String nomFournisseur);
    
   boolean existsByPrixLessThan(double prix);
   
   int countByDateProductionAfter(LocalDate date);
   
   // void deleteByPrixGreterThan(double prix);
   int deleteByPrixGreaterThan(double prix);
   
   // JPQL
   @Query("SELECT a FROM Article a WHERE a.prix<:montant")
   List<Article> getArticleByPrixJPQL(@Param("montant") double prixMax);
    
   //SQL
   @Query(nativeQuery = true,value="SELECT * FROM articles WHERE prix<?1")
   List<Article> getArticleByPrixSQL(double prixMax);
   
   // Limiter le résultat
   Article findTopByOrderByPrixDesc();
   List<Article> findTop3ByOrderByPrix();

   //Pagination
   List<Article> findByPrixLessThan(double prixMax,Pageable page);
   
   Page<Article> findByPrixGreaterThan(double prixMin,Pageable page);
}
