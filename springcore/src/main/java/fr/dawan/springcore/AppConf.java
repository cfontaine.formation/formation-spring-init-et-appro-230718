package fr.dawan.springcore;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import fr.dawan.springcore.beans.ArticleRepository;
import fr.dawan.springcore.beans.DataSource;

@Configuration // => classe de configuration
@ComponentScan(basePackages = "fr.dawan.springcore") // => scan des classes du package pour trouver les composants
                                                     // @Component, @Repository, @Controller,@Service
public class AppConf {

    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean et le nom de la méthode
    @Bean
    public DataSource datasource1() {   //On déclare un bean qui a pour nom datasource1 et qui est de type DataSource
        return new DataSource();
    }

    // L'attribut name de @Bean permet de définir le nom du bean (un ou plusieurs)
    // dans ce cas, le nom de la méthode n'est plus prix en compte
    @Bean(name = "datasource2")
 // @Primary => s'il y a plusieurs beans du même type, avec un @Autowired, c'est le bean annoté avec @Primary qui sera sélectionner
    public DataSource getData() {
        return new DataSource();
    }

    @Bean /* (initMethod = "init",destroyMethod = "destroy") */
    public ArticleRepository repository2(DataSource datasource2) {
        return new ArticleRepository(datasource2);
    }
    
    @Bean
    @Lazy
    public ArticleRepository repository3() {
        return new ArticleRepository(datasource1());
    }

    // Exercice
    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }
}
