package fr.dawan.springcore.beans;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
//import jakarta.inject.Inject;
//import jakarta.inject.Named;

@Repository("repository1")
//@Scope("prototype")
public class ArticleRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
//	@Inject
//	@Named("datasource1")
	@Autowired//(required = false)
	@Qualifier("datasource1")
	private DataSource source;


	public ArticleRepository() {
		System.out.println("Constructeur par défaut");
	}

	//@Autowired
	public ArticleRepository(/*@Qualifier("datasource1")*/ DataSource source) {
		System.out.println("Constructeur un paramètre");
		this.source = source;
	}

	public DataSource getSource() {
		return source;
	}

	//@Autowired
	public void setSource( /*@Qualifier("datasource1")*/ DataSource source) {
		System.out.println("Setter Article repository");
		this.source = source;
	}

	@Override
	public String toString() {
		return "ArticleRepository [source=" + source + ", toString()=" + super.toString() + "]";
	}
	
	@PostConstruct
	public void init() {
		System.out.println("Méthode initialisation");
	}

	@PreDestroy
	public void destroy() {
		System.out.println("Méthode destruction");
	}
}
