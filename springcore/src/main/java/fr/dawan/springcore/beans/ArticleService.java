package fr.dawan.springcore.beans;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("service1")
public class ArticleService {
	
	@Autowired
	@Qualifier("repository2")
	private ArticleRepository repository;
	
	@Autowired
	private ModelMapper map;

	public ArticleService() {

	}

	public ArticleService(ArticleRepository repository, ModelMapper map) {
		this.repository = repository;
		this.map = map;
	}

	public ArticleRepository getRepository() {
		return repository;
	}

	public void setRepository(ArticleRepository repository) {
		this.repository = repository;
	}

	public ModelMapper getMap() {
		return map;
	}

	public void setMap(ModelMapper map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return "ArticleService [repository=" + repository + ", map=" + map + "]";
	}
	
}
